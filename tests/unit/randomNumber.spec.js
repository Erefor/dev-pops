import RandomNumber from "../../src/components/RandomNumber";
import { mount } from '@vue/test-utils'

describe('Prueba de número random', () => {
    it('randomNumber default debe ser 0', () => {
        const wrapper = mount(RandomNumber)
        expect(wrapper.html()).toContain('<span>0</span>')
    })
    it('Al dar clic randomNumber debe estar entre 10 y 1', async () => {
        const wrapper = mount(RandomNumber)
        await wrapper.find('button').trigger('click')
        const randomNumber = parseInt(wrapper.find('span').text())
        expect(randomNumber).toBeGreaterThanOrEqual(1)
        expect(randomNumber).toBeLessThanOrEqual(10)

        expect(wrapper.exists()).toBe(true)
    })
    it('Se pasan props y randomNumber debe estar entre esos valores', async () => {
        const wrapper = mount(RandomNumber, {
            props: {
                min: 150,
                max: 300,
            }
        })
        await wrapper.find('button').trigger('click')
        const randomNumber = parseInt(wrapper.find('span').text())
        expect(randomNumber).toBeGreaterThanOrEqual(150)
        expect(randomNumber).toBeLessThanOrEqual(300)
    })
})
