import Header from "../../src/components/Header.vue"
import { mount } from '@vue/test-utils'

describe('HelloWorld.vue', () => {
  it('Se debe mostrar el boton', async () => {
      const wrapper = mount(Header)
      //Eventos y aplicar datos required await para generar el cambio en el DOM
      await wrapper.setData({ showHeader: true })
      expect(wrapper.find('button').exists()).toBe(true)
  })
    it('No se debe mostrar el boton', async () => {
        const wrapper = mount(Header)
        await wrapper.setData({ showHeader: false })
        expect(wrapper.find('button').exists()).toBe(false)
    });
})
